import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import lightgbm as lgbm
from sklearn.model_selection import TimeSeriesSplit
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import RandomizedSearchCV
from lightgbm import LGBMRegressor


def fill_weather_dataset(weather_df):
    # Добавляем новые признаки
    weather_df["datetime"] = pd.to_datetime(weather_df["timestamp"])
    weather_df["day"] = weather_df["datetime"].dt.day
    weather_df["month"] = weather_df["datetime"].dt.month

    # Заполним пропуски в столбце air_temperature средней месячной температурой воздуха
    air_temperature_filler = pd.DataFrame(weather_df.groupby(['site_id', 'month', 'day'])['air_temperature'].mean())
    air_temperature_nan = weather_df[weather_df['air_temperature'].isnull()].index

    for i in air_temperature_nan:
        num = (weather_df.loc[i]['site_id'], weather_df.loc[i]['month'], weather_df.loc[i]['day'])
        weather_df.loc[i, 'air_temperature'] = float(air_temperature_filler.loc[num].values)

    # Зополним пропуски в столбце cloud_coverage средним значением за день
    cloud_coverage_filler = pd.DataFrame(weather_df.groupby(['site_id', 'month', 'day'])['cloud_coverage'].mean())
    cloud_coverage_nan = weather_df[weather_df['cloud_coverage'].isnull()].index

    for i in cloud_coverage_nan:
        num = (weather_df.loc[i]['site_id'], weather_df.loc[i]['month'], weather_df.loc[i]['day'])
        weather_df.loc[i, 'cloud_coverage'] = float(cloud_coverage_filler.loc[num].values)

    # Если не известно ни одного значения за день, заполним их предыдущими известными
    weather_df['cloud_coverage'] = weather_df['cloud_coverage'].fillna(method="ffill")

    # Заполним пропуски в столбце precip_depth средним значением за месяц
    precip_depth_filler = pd.DataFrame(weather_df.groupby(['site_id', 'month'])['precip_depth_1_hr'].mean())
    precip_depth_nan = weather_df[weather_df['precip_depth_1_hr'].isnull()].index

    for i in cloud_coverage_nan:
        num = (weather_df.loc[i]['site_id'], weather_df.loc[i]['month'])
        weather_df.loc[i, 'precip_depth_1_hr'] = float(precip_depth_filler.loc[num].values)

    # Если не известно ни одного значения за день, заполним их предыдущими известными
    weather_df['precip_depth_1_hr'] = weather_df['precip_depth_1_hr'].fillna(method="ffill")
    weather_df.loc[0, 'precip_depth_1_hr'] = 0

    weather_df = weather_df.drop(['datetime', 'day', 'month'], axis=1)

    weather_df["timestamp"] = pd.to_datetime(weather_df["timestamp"])

    return weather_df

unimportant_cols = ['wind_direction', 'wind_speed', 'sea_level_pressure']
target = 'meter_reading'

weather_train_df = pd.read_csv('ashrae-energy-prediction/weather_train.csv', usecols=lambda c: c not in unimportant_cols)
weather_test_df = pd.read_csv('ashrae-energy-prediction/weather_test.csv', usecols=lambda c: c not in unimportant_cols)

weather_train_df = fill_weather_dataset(weather_train_df)
weather_test_df = fill_weather_dataset(weather_test_df)


def load_data(source, weather):
    building = pd.read_csv('ashrae-energy-prediction/building_metadata.csv',
                           dtype={'building_id': np.uint16, 'site_id': np.uint8})
    df = pd.read_csv(f'ashrae-energy-prediction/{source}.csv', parse_dates=['timestamp'])
    df = df.merge(building, on='building_id', how='left')
    df = df.merge(weather, on=['site_id', 'timestamp'], how='left')
    df = df.drop(['year_built'], axis=1)
    return df

train = load_data('train', weather_train_df)
test = load_data('test', weather_test_df)

mask1 = train["meter"] == 0
mask2 = train["meter_reading"] < 0.37
mask3 = train["meter_reading"] > 8100
mask = mask1&(mask2|mask3)
train = train.drop(train[mask].index,axis=0)

mask1 = train["meter"] == 1
mask2 = train["meter_reading"] < 0.0067
mask3 = train["meter_reading"] > 22000
mask = mask1&(mask2|mask3)
train = train.drop(train[mask].index,axis=0)

mask1 = train["meter"] == 2
mask2 = train["meter_reading"] < 1
mask3 = train["meter_reading"] > 162000
mask = mask1&(mask2|mask3)
train = train.drop(train[mask].index,axis=0)

mask1 = train["meter"] == 3
mask2 = train["meter_reading"] < 0.082
mask3 = train["meter_reading"] > 22000
mask = mask1&(mask2|mask3)
train = train.drop(train[mask].index,axis=0)

from sklearn.preprocessing import LabelEncoder


def Preprocessor(df, data_ratios):
    avgs = df.loc[:, data_ratios < 1.0].mean()
    le = LabelEncoder()
    le.fit(df["primary_use"])

    df = df.fillna(avgs)
    df['primary_use'] = np.uint8(le.transform(df['primary_use']))

    df['hour'] = np.uint8(df['timestamp'].dt.hour)
    df['day'] = np.uint8(df['timestamp'].dt.day)
    df['weekday'] = np.uint8(df['timestamp'].dt.weekday)
    df['month'] = np.uint8(df['timestamp'].dt.month)
    df['year'] = np.uint8(df['timestamp'].dt.year - 2000)

    df['floor_count'] = np.uint8(df['floor_count'])

    for col in df.columns:
        if col in ['timestamp', 'row_id']:
            del df[col]

    if 'meter_reading' in df.columns:
        df['meter_reading'] = np.log1p(df['meter_reading'] / df['square_feet']).astype(np.float32)

    return df

data_ratios = train.count()/len(train)

train_transform = Preprocessor(train, data_ratios)
train_transform = train_transform.reset_index(drop=True)

features = [col for col in train_transform.columns if col not in [target]]

X = train_transform[features]
y = train_transform[target]

model = LGBMRegressor(n_estimators = 600, learning_rate = 0.12752594220608948, num_leaves = 100, max_depth = 11)

data_ratios = test.count()/len(test)

test_transform = Preprocessor(test, data_ratios)

model.fit(X, y)

set_size = len(test_transform)
iterations = 100
batch_size = set_size // iterations

meter_reading = []
for i in range(iterations):
    pos = i*batch_size
    fold_preds = model.predict(test_transform[features].iloc[pos : pos+batch_size])
    meter_reading.extend(fold_preds)

meter_reading_exp = np.expm1(meter_reading)
meter_reading_exp = meter_reading_exp * test_transform['square_feet']

dic = {"pred": meter_reading_exp}
df_pred_out = pd.DataFrame(data=dic)

submission = pd.read_csv('ashrae-energy-prediction/sample_submission.csv')
submission['meter_reading'] = np.clip(meter_reading_exp, a_min=0, a_max=None)

submission.to_csv('submission_new_2.csv', index=False)